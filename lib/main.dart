import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is my Doge', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text("Doesn't he look goofy?", style: TextStyle(
                  color: Colors.grey[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.add_alert, color: Colors.red[500]),
          Text('23'),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;

    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
              margin: const EdgeInsets.only(top: 8),
              child: Text(
                  label,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: color,
                  )
              )
          ),
        ],
      );
    }
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.directions_run, 'Exercise'),
          _buildButtonColumn(color, Icons.card_travel, 'Travel'),
          _buildButtonColumn(color, Icons.hourglass_full, 'Patient'),
          _buildButtonColumn(color, Icons.audiotrack, 'Music')
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Meet prince, he was originally my cousins dog but they could no longer care for him so I adopted him! '
            'He the goodest boye I could ever ask for. I always love teaching him new tricks! It is so fun. '
            'Whenever I need a friend to chill with, he always has my back. I can always count on Price to be by my side. '
            'He will be my best friend forever.',
        softWrap: true,
      ),
    );
    //Color color = Theme.of(context).primaryColor;
    return MaterialApp(
        title: 'Meet my friend!',
        home: Scaffold(
            appBar: AppBar(
              title: Text('Meet My Friend'),
            ),
            body: ListView(
                children: [
                  Image.asset(
                    'images/Prince.jpg',
                    width: 800,
                    height: 300,
                    fit: BoxFit.cover,
                  ),
                  titleSection,
                  buttonSection,
                  textSection
                ]
            )
        )
    );
  }
}//myApp